# Nexplore Code Test developed by Ken Ip

## How to Run the Project

- This project relies on Docker Compose to run locally.

### Requirements:

- Git
- Docker software installed on your machine
- Ports 80, 5000, and 5432 reserved for the usage of the frontend, backend, and Postgres containers

### Installation

To run the project on your machine, execute the following commands in the command line:
```bash
git clone https://gitlab.com/apple0032/nexplore-dev.git
cd nexplore-dev
docker compose up -d
```
- After running the above commands, three containers will be created: backend, frontend, and postgres.
- A SQL file (`postgres/init.sql`) will be copied to the postgres container and executed once the container is created.
- This will create a table for the to-do app (`to_do_table`) and insert some dummy data.
- No `.env` file is required for the backend as all environment variables will be passed through the Dockerfile.

## How to run the app
Visit the following URL in your browser:
```bash
http://localhost
```
<img src="https://i.ibb.co/3CP32Yp/apps.png" width="500" alt="app image">
<img src="https://i.ibb.co/KDNVFJx/app2.png" width="500" alt="app2 image">

### Documentation
- Please visit the following URL for the backend API documentation:
```bash
http://localhost:5000/api-docs/
```
<img src="https://i.ibb.co/X26vRHJ/swagger.png" width="500" alt="swagger">

### How to run test cases
- Frontend
```
 cd todo-app
 npm run test
```
- test file location: nexplore-dev\todo-app\src\App.test.tsx
<img src="https://i.ibb.co/FXYxQgp/jest.png" width="500" alt="app image">


- Backend
```
 cd backend
 npm run test
```
- test file location: nexplore-dev\backend\test\todoController.test.ts
<img src="https://i.ibb.co/zxGF17k/backend-jest.png" width="500" alt="app image">
