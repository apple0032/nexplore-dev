import express, { Request, Response } from 'express';
import { pool } from '../utils/db';
import { TodoModel } from '../models/todoModel';
const { body,validationResult } = require('express-validator');
import ValidationException from '../exception/ValidationException';
import GeneralException from "../exception/GeneralException";


const todoRouter = express.Router();

/**
 * @swagger
 * /api:
 *   get:
 *     description: Get the list of all todos from to-do table order by id ascending
 *     summary: Get todo list
 *     tags:
 *       - todo
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully retrieved To do list of all todo items
 *       400:
 *         description: General Exception - Internal server error when retrieving all todos records
 *
 */
todoRouter.get('/', async (req: Request, res: Response, next) => {
    try {
        const todos = await TodoModel.find();
        res.json(todos);
    } catch (err : any) {
        return next(new GeneralException( [{msg: err.error}]));
    }
});

/**
 * @swagger
 * paths:
 *   /api/{id}:
 *     get:
 *       description: Get a Todo object by ID
 *       tags:
 *         - todo
 *       summary: Get a Todo object by ID
 *       parameters:
 *         - in: path
 *           name: id
 *           required: true
 *           schema:
 *             type: integer
 *           description: Searching ID of a todo object
 *       responses:
 *         200:
 *           description: Successfully retrieved Todo object by ID
 *         400:
 *           description: General Exception - Todo not found by this params id
 */
todoRouter.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const todo = await TodoModel.findById(Number(req.params.id));

        if (todo.length === 0) {
            return next(new GeneralException( [{msg: 'Todo not found by this params id'}]));
        }

        return res.json(todo);
    } catch (err : any) {
        return next(new GeneralException( [{msg: err.error}]));
    }
});


/**
 * @swagger
 * paths:
 *   /api:
 *     post:
 *       description: create a new todo object
 *       tags:
 *         - todo
 *       summary: Creat a new Todo object
 *       parameters:
 *         - name: name
 *           in: body
 *           description: the name of a todo item
 *           required: true
 *         - name: content
 *           in: body
 *           description: the content of a todo item
 *       responses:
 *         200:
 *           description: Successfully create new Todo object
 *         400:
 *           description: Validation Exception - Can not pass form validation
 */
todoRouter.post('/', body('name')
        .notEmpty().withMessage('Todo name cannot be null')
        .isLength({min: 4, max: 255}).withMessage('Todo name must have min 4 and max 255 characters'),
        body('content')
        .isLength({max: 255}).withMessage('Content length max 255 characters'),
    async (req: Request, res: Response, next) => {

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            //console.log('error:', errors)
            return next(new ValidationException(errors.array()));
        }

        try {
            const todo = new TodoModel();
            const { name, content } = req.body;
            Object.assign(todo, { name, content });
            const SavedTodos = await TodoModel.create(todo);
            return res.json(SavedTodos);
        } catch (err : any) {
            return next(new GeneralException( [{msg: err.error}]));
        }
});


/**
 * @swagger
 * paths:
 *   /api/{id}:
 *     put:
 *       description: Update a Todo object by ID
 *       tags:
 *         - todo
 *       summary: Update a Todo object by ID
 *       parameters:
 *         - in: path
 *           name: id
 *           required: true
 *           schema:
 *             type: integer
 *           description: Searching ID of a todo object
 *         - in: body
 *           name: name
 *           required: true
 *           description: the name of a todo item
 *         - in: body
 *           name: content
 *           description: the content of a todo item
 *       responses:
 *         200:
 *           description: Successfully update Todo object by ID
 *         400:
 *           description: General Exception - Todo not found by this params id  OR  Validation Exception - Can not pass form validation
 */
todoRouter.put('/:id', body('name')
        .notEmpty().withMessage('Todo name cannot be null')
        .isLength({min: 4, max: 100}).withMessage('Todo name must have min 4 and max 32 characters'),
        body('content')
        .isLength({max: 255}).withMessage('Content length max 255 characters'),
    async (req: Request, res: Response, next) => {

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return next(new ValidationException(errors.array()));
        }

        try {
            const ExistedTodo = await TodoModel.findById(Number(req.params.id));
            if (ExistedTodo.length === 0) {
                return next(new GeneralException( [{msg: 'Todo not found by this params id'}]));
            }

            const todo = new TodoModel();
            const { name, content } = req.body;
            Object.assign(todo, { name, content });
            const SavedTodos = await TodoModel.update(Number(req.params.id), todo);

            return res.json(SavedTodos);
        } catch (err : any) {
            return next(new GeneralException( [{msg: err.error}]));
        }
    });


/**
 * @swagger
 * paths:
 *   /api/{id}:
 *     delete:
 *       description: Delete a Todo object by ID
 *       tags:
 *         - todo
 *       summary: Delete a Todo object by ID
 *       parameters:
 *         - in: path
 *           name: id
 *           required: true
 *           schema:
 *             type: integer
 *           description: Searching ID of a todo object
 *       responses:
 *         200:
 *           description: Successfully delete Todo object by ID
 *         400:
 *           description: General Exception - Todo not found by this params id  OR  Validation Exception - Can not pass form validation
 */
todoRouter.delete('/:id', async (req: Request, res: Response, next) => {
    try {
        const ExistedTodo = await TodoModel.findById(Number(req.params.id));
        if (ExistedTodo.length === 0) {
            return next(new GeneralException( [{msg: 'Todo not found by this params id'}]));
        }

        const SavedTodos = await TodoModel.delete(Number(req.params.id));
        return res.json(SavedTodos);

    } catch (err : any) {
        return next(new GeneralException( [{msg: err.error}]));
    }
});





export { todoRouter };