import { Request, Response, NextFunction } from 'express';
import ValidationException from "../exception/ValidationException";
import GeneralException from "../exception/GeneralException";

interface ValidationError {
    type: string;
    msg: string;
    value: string;
    path: string;
}

export default function errorHandler(
    err: ValidationException | GeneralException,
    req: Request,
    res: Response,
    next: NextFunction
) {
    const { status, message, errors } = err;
    let errors_items: Array<string> = [];

    errors.forEach((error: ValidationError | GeneralException) => {
        errors_items.push(error.msg);
    });

    res.status(status).send({
        message,
        timestamp: Date.now(),
        path: req.originalUrl,
        method: req.method,
        errors_items,
    });

    next();
}