export default class GeneralException extends Error {
    status: number;
    msg: string;
    errors: any[];

    constructor(errors: any[]) {
        super('General Exception');
        this.name = 'General Exception';
        this.status = 400;
        this.msg = 'General Exception';
        this.errors = errors;
        Object.setPrototypeOf(this, GeneralException.prototype);
    }
}