export default class ValidationException extends Error {
    status: number;
    message: string;
    errors: any[];

    constructor(errors: any[]) {
        super('Validation Exception');
        this.name = 'ValidationException';
        this.status = 400;
        this.message = 'Validation Exception';
        this.errors = errors;
        Object.setPrototypeOf(this, ValidationException.prototype);
    }
}