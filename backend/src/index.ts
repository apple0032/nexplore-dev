import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { todoRouter } from './controllers/todoController';
import { swaggerRouter } from "./swagger";
import errorHandler from './error/ErrorHandler';
const cors = require('cors')
require('dotenv').config();
const app = express();
const PORT = process.env.APP_PORT? process.env.APP_PORT : "5000";

app.use(bodyParser.json());
app.use(cors());                //CORS setting
app.use('/api', todoRouter);    //To do control RESTFUL API
app.use("/", swaggerRouter);    //Display API documentation based on all defined route
app.use(errorHandler);          //Handle all error exception

//Catch all undefined url
app.use((req: Request, res: Response) => {
    res.status(404).send({
        error: 'Not Found',
        message: 'The requested resource could not be found.',
    });
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});