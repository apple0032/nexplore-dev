import {pool} from '../utils/db';

const TABLE_NAME = 'to_do_table'

/**
 * @swagger
 * components:
 *   schemas:
 *     Todo Object:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           description: ID of the todo object
 *           key: primary key, auto increment
 *         name:
 *           type: string
 *           description: Name of the todo
 *           required: true
 *         content:
 *           type: string
 *           description: Content of the todo
 *           required: false
 *         create_datetime:
 *           type: string
 *           description: create time
 *         update_datetime:
 *           type: string
 *           description: Update time
 *       example:
 *         id: 1
 *         name: Example Todo name
 *         content: Example Todo content
 *     Error:
 *       type: object
 *       properties:
 *         400 - Bad Request:
 *           type: object
 *           description: Bad Request when Validation Exception or General Exception throw
 *         404 - Resource not found:
 *           type: object
 *           description: The requested resource could not be found.
 *       example:
 *         message: General Exception
 *         timestamp: 1704936636652
 *         path: /api/30
 *         method: GET
 *         errors_items: ["Todo not found by this params id"]
 */

export class TodoModel {
    id: number;
    name!: string;
    content: string;
    create_datetime: Date;
    update_datetime: Date;

    constructor() {
        this.id = 0;
        this.name = '';
        this.content = '';
        this.create_datetime = new Date();
        this.update_datetime = new Date();
    }

    static async find(): Promise<TodoModel[]> {
        try {
            const { rows } = await pool.query(`SELECT * FROM ${TABLE_NAME} ORDER BY id DESC`);
            return rows as TodoModel[];
        } catch (error) {
            console.error('Error retrieving todos:', error);
            throw {error: 'Internal server error when retrieving all todos records'};
        }
    }

    static async findById(id: number): Promise<TodoModel[]> {
        try {
            const query = `SELECT * FROM ${TABLE_NAME} WHERE id = $1`; // Use parameterized query
            const values = [id]; // Pass the id as a value for the parameterized query

            const { rows } = await pool.query(query, values);
            return rows as TodoModel[];
        } catch (error) {
            console.error('Error retrieving todos:', error);
            throw {error: 'Internal server error when retrieving a todo item by id'};
        }
    }

    static async create(todo: TodoModel): Promise<TodoModel> {
        try {
            const { name, content, create_datetime, update_datetime } = todo;
            const query = `INSERT INTO ${TABLE_NAME} (name, content, create_datetime, update_datetime) VALUES ($1, $2, $3, $4) RETURNING *`;
            const values = [name, content, create_datetime, update_datetime];

            const result = await pool.query(query, values);
            return result.rows[0];
        } catch (error) {
            console.error('Error creating todo:', error);
            throw { error: 'Internal server error when creating a todo item' };
        }
    }

    static async update(id: number, todo: TodoModel) {
        try {
            const { name, content , update_datetime } = todo;
            const query = `UPDATE ${TABLE_NAME} SET name = $1, content = $2 , update_datetime = $3 WHERE id = $4  RETURNING *`;
            const values = [name, content, update_datetime, id];

            let result = await pool.query(query, values);
            return result.rows[0];
        } catch (error) {
            console.error('Error updating todo:', error);
            throw {error: 'Internal server error when updating a todo item'};
        }
    }


    static async delete(id: number) {
        try {
            const query = `DELETE FROM ${TABLE_NAME} WHERE id = $1  RETURNING *`;
            const values = [id];

            let result = await pool.query(query, values);
            return { message: 'Todo item deleted successfully' , deleted : result.rows[0]};
        } catch (error) {
            console.error('Error deleting todo:', error);
            throw { error: 'Internal server error when deleting a todo item' };
        }
    }

}