import express from "express";
const swaggerRouter = express.Router();
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Todo List App Documentation',
            version: '1.0.0',
        },
    },
    apis: [
        './src/controllers/*.ts',
        './src/models/*.ts'
    ],
};

const swaggerSpec = swaggerJSDoc(options);
swaggerRouter.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));


export { swaggerRouter };