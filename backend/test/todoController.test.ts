const request = require('supertest');
const assert = require('assert');
const express = require('express');
import  {TodoModel} from '../src/models/todoModel'
import GeneralException from '../src/exception/GeneralException'
import ValidationException from "../src/exception/ValidationException";

const newTodo = new TodoModel();
test('test find all todos', async () => {
    const todos = await TodoModel.find();
    expect(todos.length).toBeGreaterThan(0)
});

test('test find todo by id', async () => {
    const todos = await TodoModel.findById(1);
    expect(todos.length).toBeGreaterThan(0)
    expect(todos[0].id).toEqual(1)
});

test('test find todo by id fail', async () => {
    const todos = await TodoModel.findById(200);
    expect(todos.length).toEqual(0)
    expect(todos).toEqual([])
});

test('test create todo', async () => {

    const creatObject = new TodoModel();
    creatObject.name = "test"
    creatObject.content = "dummy_content"

    const todo = await TodoModel.create(creatObject);
    expect(todo).not.toBeNull();
    expect(todo.name).toEqual("test")
    expect(todo.content).toEqual("dummy_content")
    newTodo.id = todo.id
    newTodo.name = todo.name
});


test('test update todo by id', async () => {

    const updateObject = new TodoModel();
    updateObject.name = "update_test"
    updateObject.content = "update_content"

    const todo = await TodoModel.update(newTodo.id,updateObject);

    expect(todo).not.toBeNull();
    expect(todo.id).toEqual(newTodo.id)
    expect(todo.name).toEqual("update_test")
    expect(todo.content).toEqual("update_content")
});

test('test delete todo by id', async () => {
    const todo = await TodoModel.delete(newTodo.id);
    expect(todo).not.toBeNull();

    const find_todo = await TodoModel.findById(newTodo.id);
    expect(find_todo).toEqual([])
});


