CREATE TABLE to_do_table (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    content TEXT,
    create_datetime TIMESTAMP ,
    update_datetime TIMESTAMP
);

INSERT INTO to_do_table (name, content, create_datetime, update_datetime)
VALUES
    ('Buy PS5', 'Should by PS5 before christmas discount', '2022-01-01 10:15:00', '2022-01-01 12:30:00'),
    ('Do code test', 'I need to finish the code test', '2022-01-02 09:45:00', '2022-01-03 14:20:00'),
    ('Train AI model', 'keep training the AI model at this weekend', '2022-01-03 13:00:00', '2022-01-04 16:45:00');
