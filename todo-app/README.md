# To-do App developed with Reactjs

This project was develop with Reactjs by Ken Ip

## How to run the project

```
npm install
npm run start
```

## How to run test case by jest

```
npm run test
npm run test:cov
```

