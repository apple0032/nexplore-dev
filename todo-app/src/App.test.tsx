import React from 'react';
import {render, screen, fireEvent, waitFor, getByText} from '@testing-library/react';
import App from './App';
import { jest } from '@jest/globals';

window.matchMedia = window.matchMedia || function() {
  return {
    matches: false,
    addListener: function() {},
    removeListener: function() {}
  };
};

test('App is running correctly', () => {
  render(<App />);
  const linkElement = screen.getByText(/Todo App/i);
  expect(linkElement).toBeInTheDocument();
});

test('display error message when validation fail', async () => {
  render(<App />);

  fireEvent.click(screen.getByText('Add new item'));
  expect(screen.getByText('Add a new to-do item')).toBeInTheDocument();

  fireEvent.change(screen.getByPlaceholderText('Enter a todo name'), { target: { value: '' } });
  fireEvent.click(screen.getByText('Add new to-do'));

  await waitFor(() => {
    expect(screen.getByText('name is required, name must be at least 4 characters,')).toBeInTheDocument();
  });

});

test('adds a new todo when "Add new item" button is clicked', async () => {
  render(<App />);

  // Find and click the "Add new item" button
  fireEvent.click(screen.getByText('Add new item'));

  expect(screen.getByText('Add a new to-do item')).toBeInTheDocument();
  expect(screen.getByPlaceholderText('Enter a todo name')).toBeInTheDocument();
  expect(screen.getByPlaceholderText('Content of to-do')).toBeInTheDocument();

  fireEvent.change(screen.getByPlaceholderText('Enter a todo name'), { target: { value: 'New Todo' } });
  fireEvent.change(screen.getByPlaceholderText('Content of to-do'), { target: { value: 'some dummy content' } });
  fireEvent.click(screen.getByText('Add new to-do'));

  await waitFor(() => {
    expect(screen.queryAllByText('New Todo').length).toBeGreaterThan(0);
  });

})

test('update a todo item when validation fail', async () => {
  render(<App />);
  await waitFor(() => {
    expect(screen.getAllByText('New Todo').length).toBeGreaterThan(0);
  });

  const buttons = screen.getAllByRole('button');
  const updateButtons = buttons.filter((button) => {
    return button!.textContent!.includes('Update');
  });
  fireEvent.click(updateButtons[0]);

  await waitFor(() => {
    expect(screen.getByText('Update Todo item')).toBeInTheDocument();
  });

  fireEvent.change(screen.getByPlaceholderText('Enter a todo name'), { target: { value: '' } });
  fireEvent.click(screen.getByText('Update to-do'));

  await waitFor(() => {
    expect(screen.getByText('name is required, name must be at least 4 characters,')).toBeInTheDocument();
  });
})


test('update a todo item', async () => {
  render(<App />);
  await waitFor(() => {
    expect(screen.getAllByText('New Todo').length).toBeGreaterThan(0);
  });

  const buttons = screen.getAllByRole('button');
  const updateButtons = buttons.filter((button) => {
    return button!.textContent!.includes('Update');
  });
  fireEvent.click(updateButtons[0]);

  await waitFor(() => {
    expect(screen.getByText('Update Todo item')).toBeInTheDocument();
  });

  fireEvent.change(screen.getByPlaceholderText('Enter a todo name'), { target: { value: 'Update Todo' } });
  fireEvent.click(screen.getByText('Update to-do'));

  await waitFor(() => {
    expect(screen.queryAllByText('Update Todo').length).toBeGreaterThan(0);
  });
})


test('delete a todo item', async () => {
  render(<App />);
  await waitFor(() => {
    expect(screen.getAllByText('Update Todo').length).toBeGreaterThan(0);
  });

  const buttons = screen.getAllByRole('button');
  const updateButtons = buttons.filter((button) => {
    return button!.textContent!.includes('Delete');
  });
  fireEvent.click(updateButtons[0]);

  await waitFor(() => {
    expect(screen.queryAllByText('Update Todo').length).toBe(0);
  });
})