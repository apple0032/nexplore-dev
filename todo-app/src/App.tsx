import React, { useState, useEffect } from 'react';
import { Form, Input, Button, List, Row, Col, Modal ,notification  } from 'antd';
import {
  PlusOutlined,
  EditOutlined,
  FileAddOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
  InfoCircleOutlined
} from '@ant-design/icons';
import { Formik, Field } from 'formik';
import TodoItem from "./component/TodoItem";
import { Todo } from './interfaces/Todo';
import ApiService from '../src/services/apiService';
import moment from 'moment';
import * as Yup from 'yup';
import "./App.css";
import apiService from "../src/services/apiService";


const { TextArea } = Input;

const TodoApp: React.FC = () => {

  const initialTodo: Todo = {
    id : 0,
    name: "",
    content: "",
    create_datetime: "",
    update_datetime: ""
  };
  const [todos, setTodos] = useState<Todo[]>([]);
  const [displayTodo, setNewTodo] =  useState<Todo>(initialTodo);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [isCreate, setIsCreate ] = useState(true);
  const validationSchema = Yup.object({
    name: Yup.string().required('name is required').min(4, 'name must be at least 4 characters').max(255, 'name can be at most 255 characters'),
    content: Yup.string().max(255, 'content can be at most 255 characters'),
  });


  const showModal = () => {
    setErrorMessage("")
    setIsModalOpen(true);
  };

  const openCreate = () => {
    setIsCreate(true)
    setNewTodo({ ...initialTodo })
    showModal()
  }

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const openNotification = (status : string) => {
    if(status === 'success') {
      notification.open({
        message: 'Successful.',
        description: (
            <div>
              <CheckCircleOutlined style={{marginRight: '8px', color: '#13ff00'}}/>
              Your action was successfully done without errors.
            </div>
        ),
        duration: 3,
      });
    } else {
      notification.open({
        message: 'Error occurred!',
        description: (
            <div>
              <CloseCircleOutlined style={{marginRight: '8px', color: 'red'}}/>
              Please check your input before submit.
            </div>
        ),
        duration: 3,
      });
    }
  };
  notification.config({
    placement: 'bottomRight'
  });

  const initialValues: Todo = { ...initialTodo };

  useEffect(() => {
    (async () => {
      await fetchTodos();
    })();
  }, []);

  const fetchTodos = async () => {
    try {
      const response = await ApiService.fetchTodos();
      setTodos(response);
    } catch (error) {
      openNotification("fail");
      console.error('Error fetching todos:', error);
    }
  };

  const addTodo = async () => {

    validationSchema
        .validate(displayTodo, { abortEarly: false })
        .then(async () => {
          try {
            let res;
            if(isCreate) {
              res = await ApiService.createTodo(displayTodo);
            } else {
              res = await ApiService.updateTodo(displayTodo);
            }
            if(res.status === 200) {
              openNotification("success");
              await fetchTodos()
              setNewTodo({...initialTodo});
              setIsModalOpen(false);
            }
          } catch (error) {
            openNotification("fail");
            console.error('Error adding todo:', error);
          }
        })
        .catch((errors) => {
          openNotification("fail");
          let errorMsg: string = "";
          (errors.errors).forEach((err: string, index: number) => {
            errorMsg += err;
            if (index < errors.errors.length - 1) {
              errorMsg += ", ";
            }
          });
          setErrorMessage(errorMsg)
        });

  };

  const openTodo = async (id: number,) => {
    try {
      const res = await apiService.findTodoByID(id)
      if(res.status === 200) {
        setNewTodo(res.data[0]);
        setIsCreate(false)
        showModal()
      }
    } catch (error) {
      openNotification("fail");
    }
  };

  const deleteTodo = async (id: number) => {
    try {
      const res = await apiService.deleteTodoByID(id)
      if(res.status === 200) {
        openNotification("success");
        await fetchTodos()
      }
    } catch (error) {
      console.error('Error deleting todo:', error);
    }
  };

  const onSubmit = async (values: { name: string }, { resetForm }: any) => {
    resetForm();
    await fetchTodos();
  };

  return (
      <div className="todos-container">
        <h1>
          Todo App
          <div className="todos-title">
            <EditOutlined />Manage your to do items in one page
          </div>
        </h1>

        <Button type="primary" onClick={openCreate}>
          <FileAddOutlined /> Add new item
        </Button>

        <Modal title={isCreate ? "Add a new to-do item" : "Update Todo item"} open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width={800} footer="">
          <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={onSubmit}
          >
            <Form>
              <Row>
                <Col span={24}>
                  <div>
                    Name:
                    <Field
                        type="text"
                        name="name"
                        placeholder="Enter a todo name"
                        as={Input}
                        value={displayTodo.name}
                        onChange={(e: any) => setNewTodo({ ...displayTodo, name: e.target.value })}
                    />
                    <br/><br/>
                    Content:
                    <TextArea
                        showCount
                        name="content"
                        maxLength={255}
                        placeholder="Content of to-do"
                        style={{ height: 100, resize: 'none' }}
                        value={displayTodo.content}
                        onChange={(e: any) => setNewTodo({ ...displayTodo, content: e.target.value })}
                    />
                  </div>
                  {!isCreate && (
                      <div className="todo-update-datetime">
                        Last update time: {moment(displayTodo.update_datetime).format("YYYY-MM-DD HH:mm:ss")}
                      </div>
                  )}
                  {errorMessage !== "" && (
                      <div className="form-errors">
                        <InfoCircleOutlined />
                        {errorMessage}
                      </div>
                  )}
                </Col>
                <Col span={10}>
                  {isCreate ? (
                      <Button className="submit-button" type="primary" htmlType="submit" icon={<PlusOutlined />} onClick={() => addTodo()}>
                        Add new to-do
                      </Button>
                  ) : (
                      <Button className="submit-button" style={{ backgroundColor: '#389e0d', color: 'white' }} htmlType="submit" icon={<PlusOutlined />} onClick={() => addTodo()}>
                        Update to-do
                      </Button>
                  )}
                </Col>
              </Row>
            </Form>
          </Formik>
        </Modal>
        <List
            dataSource={todos}
            renderItem={(todo: Todo) => (
                <TodoItem
                    todo={todo}
                    openTodo={openTodo}
                    deleteTodo={deleteTodo}
                />
            )}
        />
      </div>
  );
};

export default TodoApp;