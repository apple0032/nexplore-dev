import React from 'react';
import { List, Button } from 'antd';
import { Todo } from '../interfaces/Todo';

interface TodoItemProps {
    todo: Todo;
    openTodo: (id: number) => void;
    deleteTodo: (id: number) => void;
}

const TodoItem: React.FC<TodoItemProps> = ({ todo, openTodo, deleteTodo }) => {
    return (
        <List.Item key={todo.id}>
        <div>
            <div className="todos-name">{todo.name} <br /></div>
            <div className="todos-content">{todo.content || ""}</div>
        </div>
    <div>
        <Button onClick={() => openTodo(todo.id)} className="update-btn">Update</Button>
        <Button onClick={() => deleteTodo(todo.id)}>Delete</Button>
    </div>
    </List.Item>
);
};

export default TodoItem;