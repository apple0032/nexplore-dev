export interface Todo {
    id: number;
    name: string;
    content: string;
    create_datetime: string;
    update_datetime: string;
}