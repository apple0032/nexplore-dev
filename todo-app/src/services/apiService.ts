import axios from 'axios';
import {API_URL} from '../env'
import {Todo} from '../interfaces/Todo'

class ApiService {
    async fetchTodos(){
        try {
            const response = await axios.get(`${API_URL}`);
            return response.data;
        } catch (error) {
            throw error;
        }
    };

    async updateTodo(displayTodo: Todo){
        try {
            return await axios.put(`${API_URL}/${displayTodo.id}`, {
                name: displayTodo.name,
                content: displayTodo.content
            });
        } catch (error) {
            throw error;
        }
    }

    async createTodo(displayTodo: Todo){
        try {
            return await axios.post(`${API_URL}`, {
                name: displayTodo.name,
                content: displayTodo.content
            });
        } catch (error) {
            throw error;
        }
    }

    async findTodoByID(id: number){
        try {
            return await axios.get(`${API_URL}/${id}`, {});
        } catch (error) {
            throw error;
        }
    }

    async deleteTodoByID(id: number){
        try {
            return await axios.delete(`${API_URL}/${id}`, {});
        } catch (error) {
            throw error;
        }
    }
}

const apiService = new ApiService();
export default apiService;